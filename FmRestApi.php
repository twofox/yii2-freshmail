<?php

namespace twofox\freshmail;

use yii\base\ErrorException;
use yii;

/**
 *  Klasa do uwierzytelniania i wysyłania danych za pomocą REST API FreshMail
 *
 *  @author Tadeusz Kania, Piotr Suszalski
 *  @since  2012-06-14
 *
 */

class FmRestApi
{

    private $ApiSecret   = null;
    private $ApiKey      = null;
    private $response    = null;
    private $rawResponse = null;
    private $httpCode    = null;
    private $contentType = 'application/json';

    const host   = 'https://api.freshmail.com/';
    const prefix = 'rest/';
    //--------------------------------------------------------------------------

    /**
     * Metoda pobiera kody błędów
     *
     * @return array
     */
    public function getErrors()
    {
        if ( isset( $this->errors['errors'] ) ) {
            return $this->errors['errors'];
        }

        return false;
    }

     /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

     /**
     * @return array
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

     /**
     * @return array
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    /**
     * Metoda ustawia secret do API
     *
     * @param type $strSectret
     * @return rest_api
     */
    public function setApiSecret( $strSectret = '' )
    {
        $this->ApiSecret = $strSectret;
    } // setApiSecret

    public function setContentType( $contentType = '' )
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * Metoda ustawia klucz do API
     *
     * @param string $strKey
     * @return rest_api
     */
    public function setApiKey ( $strKey = '' )
    {
        $this->ApiKey = $strKey;
    } // setApiKey

    public function doRequest( $strUrl, $arrParams = array(), $boolRawResponse = false )
    {
        if ( empty($arrParams) ) {
            $strPostData = '';
        } elseif ( $this->contentType == 'application/json' ) {
            $strPostData = json_encode( $arrParams );
        } elseif ( !empty($arrParams) ) {
            $strPostData = http_build_query( $arrParams );
        }

        $strSign = sha1( $this->ApiKey . '/' . self::prefix . $strUrl . $strPostData . $this->ApiSecret );        

        $arrHeaders = array();
        $arrHeaders[] = 'X-Rest-ApiKey: ' . $this->ApiKey;
        $arrHeaders[] = 'X-Rest-ApiSign: ' . $strSign;

        if ($this->contentType) {
            $arrHeaders[] = 'Content-Type: '.$this->contentType;
        }

        $resCurl = curl_init( self::host . self::prefix . $strUrl );
        curl_setopt( $resCurl, CURLOPT_HTTPHEADER, $arrHeaders );
        curl_setopt( $resCurl, CURLOPT_HEADER, false );
        curl_setopt( $resCurl, CURLOPT_RETURNTRANSFER, true);

        if ($strPostData) {
            curl_setopt( $resCurl, CURLOPT_POST, true);
            curl_setopt( $resCurl, CURLOPT_POSTFIELDS, $strPostData );
        } // endif

        $this->rawResponse = curl_exec($resCurl);
        $this->httpCode = curl_getinfo($resCurl, CURLINFO_HTTP_CODE);

        $this->response = json_decode($this->rawResponse, true);
        if ($this->httpCode != 200) {
            $this->errors = $this->response['errors'];
            return false;
        } // endif

        return true;
    } // doRequest

}
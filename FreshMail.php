<?php
namespace twofox\freshmail;

use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;

//include_once __DIR__.'/FmRestApi.php';

class FreshMail extends Component {
    public $ApiSecret = null;
    public $ApiKey = null;
    public $SubscriberList = null;
    private $Freshmail = null;

    public function init() {
        $this -> Freshmail = new FmRestApi();
        $this -> Freshmail -> setApiSecret($this -> ApiSecret);
        $this -> Freshmail -> setApiKey($this -> ApiKey);
    }

    public function addSubscriber($email) {
        $data = array(
                   'email' => $email, 
                   'list' => $this -> SubscriberList, 
                   //'custom_fields' => ['personalization_tag_1' => 'value 1', 'personalization_tag_2' => 'value 2'], 
                   //'state' => 1, 
                   //'confirm' => 1, 
                   );

        return $this -> Freshmail -> doRequest('subscriber/add', $data);
    }

}
